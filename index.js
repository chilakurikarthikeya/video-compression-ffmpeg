
const express = require('express');
const bodyParser = require('body-parser')
var ffmpeg = require('ffmpeg');
const path = require('path');
const multer = require('multer');
const router = express.Router();


const app= express();


const PORT =process.env.PORT || 8080;

app.use('/', router);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

var formidable = require('formidable');
var fs = require('fs');
 
app.get('/',async (req,res)=>{
    // res.sendFile('./src/index.html');
    res.sendFile(path.join(__dirname+'/src/index.html'));


})
app.get('/video', function(req, res) {
    const path = 'video/compressed.mp4'
    const stat = fs.statSync(path)
    const fileSize = stat.size
    const range = req.headers.range
    if (range) {
      const parts = range.replace(/bytes=/, "").split("-")
      const start = parseInt(parts[0], 10)
      const end = parts[1] 
        ? parseInt(parts[1], 10)
        : fileSize-1
      const chunksize = (end-start)+1
      const file = fs.createReadStream(path, {start, end})
      const head = {
        'Content-Range': `bytes ${start}-${end}/${fileSize}`,
        'Accept-Ranges': 'bytes',
        'Content-Length': chunksize,
        'Content-Type': 'video/mp4',
      }
      res.writeHead(206, head);
      file.pipe(res);
    } else {
      const head = {
        'Content-Length': fileSize,
        'Content-Type': 'video/mp4',
      }
      res.writeHead(200, head)
      fs.createReadStream(path).pipe(res)
    }
  });
app.post('/compress',async (req,res)=>{

    storeVideo(req)
    .then(data => {
        try {
         
            var process = new ffmpeg('./video.mp4');
            process.then(function (video) {
                
                video
                // .setVideoSize('640x?', true, true, '#fff')
                // .setAudioCodec('libfaac')
                // .setAudioChannels(2)
                // .setVideoSize('40x40', false, false)
                // video.setVideoSize('640x?', true, true, '#fff');
                video.setVideoSize('360x360', false, true)


                .save('./video/compressed.mp4', function (error, file) {
                    if (!error){
                        res.send(file)
                        console.log('Video file: ' + file);
                    }

                      
                    else{
                        res.send(error)
                    }
                });
        
            }, function (err) {
                console.log('Error: ' + err);
            });
        } catch (e) {
            console.log(e.code);
            console.log(e.msg);
        }
    })
    .catch(err => {
        res.send(err)
    })
        
//   res.send(result);
    
   return 0;

   

})

const storeVideo =(req)=> new Promise((resolve, reject) => {
    var form = new formidable.IncomingForm();
    form.parse(req, function (err, fields, files) {
        var oldpath = files.filetoupload.path;
        var newpath = './video/' + files.filetoupload.name;
      
       fs.rename(oldpath, newpath,  (err)=> {
         
        if (err) reject (err);
        resolve(newpath)

        });
   });

  })

function storeTemp(req){

    var form = new formidable.IncomingForm();
    form.parse(req, function (err, fields, files) {
        var oldpath = files.filetoupload.path;
        var newpath = './video/' + files.filetoupload.name;
      
       fs.rename(oldpath, newpath,  (data,err)=> {
         
            if(data){
                return newpath
            }
            else{

                return  err
            }
        });
   });
}

app.get('/video', function(req, res) {
  const path = 'assets/sample.mp4'
  const stat = fs.statSync(path)
  const fileSize = stat.size
  const range = req.headers.range
  if (range) {
    const parts = range.replace(/bytes=/, "").split("-")
    const start = parseInt(parts[0], 10)
    const end = parts[1] 
      ? parseInt(parts[1], 10)
      : fileSize-1
    const chunksize = (end-start)+1
    const file = fs.createReadStream(path, {start, end})
    const head = {
      'Content-Range': `bytes ${start}-${end}/${fileSize}`,
      'Accept-Ranges': 'bytes',
      'Content-Length': chunksize,
      'Content-Type': 'video/mp4',
    }
    res.writeHead(206, head);
    file.pipe(res);
  } else {
    const head = {
      'Content-Length': fileSize,
      'Content-Type': 'video/mp4',
    }
    res.writeHead(200, head)
    fs.createReadStream(path).pipe(res)
  }
});
app.listen(PORT,()=>{
    console.log(`server running at PORT ${PORT}`)
})